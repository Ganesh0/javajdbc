package com.example.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.model.Employee;
import com.example.service.EmployeeServiceImpl;

@Controller
@RequestMapping("/employee")
public class EmployeeController implements ErrorController {
	
	 @Autowired
	 private EmployeeServiceImpl employeeService;
	 
	 @RequestMapping(value= {"/", "/list"}, method=RequestMethod.GET)
	 public ModelAndView getAllEmployees() {
		 System.out.println("==========working==========================");
	  ModelAndView model = new ModelAndView();
	  List<Employee> list = employeeService.getAllEmployees();
	  
	  model.addObject("employee_list", list);
	  model.setViewName("employee_list");
	  return model;
	 }
	 
	 @RequestMapping(value="/update/{id}", method=RequestMethod.GET)
	 public ModelAndView editEmployee(@PathVariable int id) {
	  ModelAndView model = new ModelAndView();
	  
	  Employee employee = employeeService.findEmployeeById(id);
	  model.addObject("employeeForm", employee);
	  
	  model.setViewName("employee_form");
	  return model;
	 }
	 
	 @RequestMapping(value="/add", method=RequestMethod.GET)
	 public ModelAndView addEmployee() {
	  ModelAndView model = new ModelAndView();
	  
	  Employee employee = new Employee();
	  model.addObject("employeeForm", employee);
	  
	  model.setViewName("employee_form");
	  return model;
	 }
	 
	 @RequestMapping(value="/save", method=RequestMethod.POST)
	 public ModelAndView saveOrUpdate(@ModelAttribute("employeeForm") Employee employee) {
	  if(employee.getEmployeeId() != null) {
			 System.out.println("======================save===========================");

	   employeeService.updateEmployee(employee);
	  } else {
			 System.out.println("======================save===========================" + employee);

	   employeeService.addEmployee(employee);
	  }
	  
	  return new ModelAndView("redirect:/employee/list");
	 }
	 
	 @RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
	 public ModelAndView deleteEmployee(@PathVariable("id") int id) {
	  employeeService.deleteEmployee(id);
	  
	  return new ModelAndView("redirect:/employee/list");
	 }
	 
	 @RequestMapping("/error")
		public ModelAndView handleError(HttpServletResponse response) {
			ModelAndView modelAndView = new ModelAndView();

			if(response.getStatus() == HttpStatus.NOT_FOUND.value()) {
				modelAndView.setViewName("error-404");
			}
			else if(response.getStatus() == HttpStatus.FORBIDDEN.value()) {
				modelAndView.setViewName("error-403");
			}
			else if(response.getStatus() == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
				modelAndView.setViewName("error-500");
			}
			else {
				modelAndView.setViewName("error");
			}

			return modelAndView;
		}


	@Override
	public String getErrorPath() {
		// TODO Auto-generated method stub
		return "/error";
	}

}
